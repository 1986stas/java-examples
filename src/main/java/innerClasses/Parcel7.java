package innerClasses;

public class Parcel7 {
    public Contents contents(){
        return new Contents(){
            private int i = 11;
            public int value(){
                return i;
            }
        };
    }
}
