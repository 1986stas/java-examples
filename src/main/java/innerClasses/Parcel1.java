package innerClasses;

public class Parcel1 {
    class Contetnts{
        private int i = 11;
        public int value(){
            return i;
        }
    }
    class Destination{
        private String label;
        Destination(String whereTo){
            label = whereTo;
        }
        String readLabel(){
            return label;
        }
    }
    private void ship(String dest){
        Contetnts contetnts = new Contetnts();
        Destination destination = new Destination(dest);
        System.out.println(destination.readLabel());
    }

    public static void main(String[] args){
        Parcel1 p = new  Parcel1();
        p.ship("Ya mama");
    }
}
