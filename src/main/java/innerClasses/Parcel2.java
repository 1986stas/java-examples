package innerClasses;

public class Parcel2 {
    class Contetnts{
        private int i = 11;
        public int value(){
            return i;
        }
    }


    class Destination{
        private String label;
        Destination(String whereTo){
            label = whereTo;
        }
        String readLabel(){
            return label;
        }
    }

    public Destination to(String s){
        return new Destination(s);
    }

    public Contetnts contetnts(){
        return new Contetnts();
    }

    public void ship(String s){
        Contetnts contetnts = contetnts();
        Destination destination = to(s);
        System.out.println(destination.readLabel() + " " + contetnts.getClass().getSimpleName());
    }

    public static void main(String[] args){
        Parcel2 p = new Parcel2();
        p.ship("Manchester City");
        Parcel2 parcel2 = new Parcel2();
        Parcel2.Contetnts c = parcel2.contetnts();
        Parcel2.Destination d = parcel2.to("Shakhtar");
    }
}
