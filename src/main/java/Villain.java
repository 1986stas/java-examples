public class Villain {

    private String name;

    protected void set(String nm) {
        this.name = nm;
    }

    public Villain(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "I'm an object Villain{ and my" +
                "name=' is " + name + '\'' +
                '}';
    }
}

class Orc extends Villain {


    private int orcNumber;

    public Orc(String name, int orcNumber) {
        super(name);
        this.orcNumber = orcNumber;
    }

    public void change(String name, int orcNumber) {
        set(name);
        this.orcNumber = orcNumber;
    }

    public String toString() {
        return "Orc " + orcNumber + ": " + super.toString();
    }

    public static void main(String[] args) {
        Orc orc = new Orc("Limburger", 12);
        System.out.println(orc);
        orc.change("Stan", 20);
        System.out.println(orc);
    }
}
