public class Game {

    Game() {
        int i = 111;
        System.out.println("I'm a Game " + i);
    }
}

class BoardGame {

    private String s = "BoardGame BoardGame";

    BoardGame() {
        System.out.println("I'm a BoardGame");
    }

    @Override
    public String toString() {
        return "BoardGame{" +
                "s='" + s + '\'' +
                '}';
    }
}

class Chess extends Game {

    private BoardGame boardGame;

    Chess() {
        super();
        System.out.println("I'm back");
        boardGame = new BoardGame();

    }


    public static void main(String[] args) {
        Chess chess = new Chess();
        BoardGame boardGame = new BoardGame();
        System.out.println(boardGame);
        System.out.println(chess);
    }
}