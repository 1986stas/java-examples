

public class Soap {

    private String s;

    public Soap() {
        System.out.println("Soap()");
        s = "Constructed";
    }

    @Override
    public String toString() {
        return s;
    }

}

class Bath {

    private String s1 = "Lucky";
    private String s2 = "Lucky", s3, s4;


    private Soap castille;
    private int i;
    private float toy;

    Bath() {
        System.out.println("В конструкторе Bath()");
        s3 = "Happy";
        toy = (float) 3.14;
        castille = new Soap();
    }

    {
        i = 47;
    }

    @Override
    public String toString() {
        if (s4 == null)
            s4 = "Радостный";
        return  "s1 = " + s1 + "\n" +
                "s2 = " + s2 + "\n" +
                "s3 = " + s3 + "\n" +
                "i = " + i + "\n" +
                "toy = " + toy + "\n" + s4 + "\n" +
                "castille = " + castille;
    }
    public static void main(String[] args){
        Bath bath = new Bath();
        System.out.println(bath);
    }
}
