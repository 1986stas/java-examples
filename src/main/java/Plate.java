public class Plate {

    public Plate(int i) {
        System.out.println(" Plate()");

    }
}

    class DinnerPlate extends Plate {
        public DinnerPlate(int i) {
            super(i);
            System.out.println(" DinnerPlate()");
        }
    }


   class Utensil {
        Utensil(int i) {
            System.out.println(" Utensil()");
        }
    }

    class Spoon extends Utensil {

        Spoon(int i) {
            super(i);
            System.out.println(" Spoon()");
        }
    }

    class Fork extends Utensil {

        Fork(int i) {
            super(i);
            System.out.println(" Fork()");
        }
    }

   class Knife extends Utensil {

        Knife(int i) {
            super(i);
            System.out.println(" Knife()");
        }
    }

   class Custom {
        Custom(int i) {
            System.out.println(" Custom()");
        }
    }

class PlaceSettings extends Custom {

    private Spoon sp;
    private Fork frk;
    private Knife kn;
    private DinnerPlate pl;

    PlaceSettings(int i) {
        super(i + 1000);
        sp = new Spoon(i + 1);
        frk = new Fork(i + 2);
        kn = new Knife(i + 3);
        pl = new DinnerPlate(i + 4);
        System.out.println(" PlaceSettings()!!!");
    }
    public static void main(String[] args) {
        PlaceSettings placeSettings = new PlaceSettings(10);

    }
}

