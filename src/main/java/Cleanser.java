public class Cleanser {

    private String s = "Cleanser";

    public void append (String a) {
        s += a;
    }
    public void dilute () {
        append(" dilute() test");
    }
    public void apply () {
        append(" apply() test 2");
    }
    public void scrub () {
        append(" scrub()");
    }


    @Override
    public String toString() {
        return s;
    }
    public static void main(String[] args){
       Cleanser cleanser = new Cleanser();
       cleanser.dilute();
       cleanser.apply();
       cleanser.scrub();
       System.out.println(cleanser);

    }
}
class Detergent extends Cleanser{


    @Override
    public void scrub(){
        append(" Detergent.scrub()");
        super.scrub();
    }

    public void foam(){
        append(" foam()");
    }

    public static void main(String[] args){
        Detergent detergent = new Detergent();
        detergent.dilute();
        detergent.apply();
        detergent.scrub();
        detergent.foam();
        System.out.println(detergent);
        Cleanser.main(args);
    }

}
class Bast {

    private Cleanser cleanser = new Cleanser();

    private String b = "I'm a Bast";

    public void scrub(){
        cleanser.scrub();
        cleanser.append(" Bast.scrub()");
    }

    public void sterilize(){
        cleanser.append(" sterilize()");
    }

    @Override
    public String toString() {
        return b;
    }

    public static void main(String[] args){
        Bast bast = new Bast();
        bast.scrub();
        bast.sterilize();
        System.out.println(bast);
    }
}



