package packageStasIsSoftwareDeveloper;

import java.util.Arrays;

public class Processor {

    public String name(){
        return getClass().getSimpleName();
    }

    Object process(Object input){
        return input;
    }
}

class Upcase extends Processor{
    String process(Object i){
        return ((String)i).toUpperCase();
    }
}

class Downcase extends Processor{
    String process(Object i){
        return ((String)i).toLowerCase();
    }
}

class Splitter extends Processor{
    String process(Object i){
        return Arrays.toString(((String) i).split(" "));
    }
}
class Apply{
    public static void process(Processor p, Object s){
        System.out.println("Using Processor " + p.name());
        System.out.println(p.process(s));
    }
    public static String s = "Something incorrect, I couldn't recognize";

    public static void main(String[] args) {
        process(new Upcase(), s);
        process(new Downcase(), s);
        process(new Splitter(), s);
    }
}
