package packageStas;

public abstract class Instrument {
    public abstract void play(Note n);

    public abstract void adjust();

    protected String what(){return "Instrument";}
}
class Wind extends Instrument{

    @Override
    public void play(Note n) {
        System.out.println(this + "Wind.play() " + n);
    }

    @Override
    public void adjust() {
    }
}
class Percussion extends Instrument{

    @Override
    public void play(Note n) {
        System.out.println("Percussion.play() " + n);
    }

    @Override
    public void adjust() {

    }

    @Override
    protected String what() {
        return "Percussion";
    }
}

interface Test{
    void test(Note note);
}

class Stas implements Test{

    @Override
    public void test(Note note) {
        System.out.println(this + ".test() " + note);
    }
}

class Brass extends Wind{
    @Override
    public void play(Note n) {
        System.out.println("Brass.play() " + n);
    }
}

class Music{

    public static void tune(Instrument i){
        i.play(Note.MIDDLE_C);
    }

    protected static void tuneAll(Instrument[] e){
        for (Instrument i : e){
            tune(i);
        }
    }

    public static void main (String[] args){
        Instrument[] instrument = {
             new Wind(), new Percussion(), new Brass()
        };
        tuneAll(instrument);

        Stas stas = new Stas();
        stas.test(Note.MIDDLE_C);
    }
}