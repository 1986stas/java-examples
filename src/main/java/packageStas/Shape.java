package packageStas;

import java.util.Random;

public class Shape {
    public void draw() {
    }

    public void erase() {
        System.out.println("I'm Shape, mother fucker ");
    }


}

class Circle extends Shape {
    @Override
    public void draw() {
        System.out.println("Circle.draw() ");
    }

    @Override
    public void erase() {
        System.out.println("Circle.erase() ");
    }
}

class Square extends Shape {
    @Override
    public void draw() {
        System.out.println("Square.draw() ");
    }

    @Override
    public void erase() {
        System.out.println("Square.erase() ");
    }
}

class Triangle extends Shape {
    @Override
    public void draw() {
        System.out.println("Triangle.draw() ");
    }

    @Override
    public void erase() {
        super.erase();
        System.out.println("Triangle.erase() ");
    }
}

class RandomShapeGenerator {
    private Random rand = new Random(50);

    public Shape next() {
        switch (rand.nextInt(3)) {
            default:
            case 0:
                return new Circle();
            case 1:
                return new Square();
            case 2:
                return new Triangle();
        }
    }
}
class Shapes {
    private static RandomShapeGenerator rm = new RandomShapeGenerator();
    public static void main (String[] args){
        Shape [] shapes = new Shape[9];
        for (int i = 0; i < shapes.length; i++){
            shapes[i] = rm.next();
        }
        for (Shape shp : shapes){
            shp.draw();
            shp.erase();
        }
    }
}