package packageStas;

public class Shared {

    private int refcount = 0;
    private static long counter = 0;
    private final long id = counter++;

    Shared() {
        System.out.println("Creating, I'm Shared " + this);
    }

    public void addRef() {
        refcount++;
    }

    protected void dispose() {
        if (--refcount == 0) {
            System.out.println("dispose method form Shared object " + this);
        }
    }

    public String toString() {
        return "Shared string " + id;
    }
}

class Composing {
    private Shared shared;
    private static long counter = 0;
    private final long id = counter++;

    Composing(Shared shared) {
        System.out.println("Creating, I'm Composing " + this);
        this.shared = shared;
        this.shared.addRef();
    }

    protected void dispose() {
        System.out.println("dispose method form Composing object " + this);
        shared.dispose();
    }

    public String toString() {
        return "Composing string " + id;
    }
}

class ReferenceCounting {

    protected void finalize() throws Throwable {
        super.finalize();
    }

    public static void main(String[] args) {
        Shared shared = new Shared();
        Composing[] composings = {new Composing(shared), new Composing(shared),
                new Composing(shared), new Composing(shared), new Composing(shared)};
        for (Composing c : composings) {
            c.dispose();
        }
        ReferenceCounting referenceCounting = new ReferenceCounting();
        try {
            referenceCounting.finalize();
            System.out.println("test");
        } catch (Throwable throwable) {
            System.out.println("test 1");
        }
    }
}
