package packageStas;

public class Characteristic {
    private String s;
    Characteristic(String s){
        this.s = s;
        System.out.println("Hello, I'm Characteristic " + s);
    }

    protected void dispose(){
        System.out.println("Завершаем I'm Characteristic " + s + "1");
    }
}

class Description {
    private String s;
    Description(String s){
        this.s = s;
        System.out.println("Hello, I'm Description " + s);
    }

    protected void dispose(){
        System.out.println("Завершаем I'm Description " + s + "1");
    }
}

class LivingCreature{
    private Characteristic p = new Characteristic("I'm alive ");
    private Description d = new Description("simple creature");
    LivingCreature(){
        System.out.println("Hello, I'm LivingCreature ");
    }

    protected void dispose(){
        System.out.println("dispose in the LivingCreature ");
        p.dispose();
        d.dispose();
    }
}
class Animal extends LivingCreature{
    private Characteristic p = new Characteristic("имеет сердце");
    private Description d = new Description("животное, а не растение");
    Animal(){
        System.out.println("Hello, I'm Animal ");
    }
    protected void dispose(){
        System.out.println("dispose() in the Animal ");
        p.dispose();
        d.dispose();
        super.dispose();
    }
}
class Amphibian extends Animal{
    private Characteristic p = new Characteristic("может жить в воде");
    private Description d = new Description("и в воде и на земле");
    Amphibian(){
        System.out.println("Hello, I'm Amphibian ");
    }
    protected void dispose(){
        System.out.println("dispose() in the Amphibian ");
        p.dispose();
        d.dispose();
        super.dispose();
    }
}
class Frog extends Amphibian{
    private Characteristic p = new Characteristic("квакает");
    private Description d = new Description("есть жуков");
    Frog(){
        System.out.println("Hello, I'm Frog ");
    }
    protected void dispose(){
        System.out.println("dispose() in the Frog ");
        p.dispose();
        d.dispose();
        super.dispose();
    }
    public static void main(String[] args) {
        Frog frog = new Frog();
        System.out.println("See u Stas ");
        frog.dispose();
    }
}