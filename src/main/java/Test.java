

class SomeThing implements Runnable {

    @Override
    public void run()
    {
        System.out.println("Привет из побочного потока!");
    }
}

public class Test
{
    static SomeThing mThing;

    public static void main(String[] args)
    {
        mThing = new SomeThing();

        Thread myThready = new Thread(mThing);
        myThready.start();

        System.out.println("Главный поток завершён...");
    }

}



