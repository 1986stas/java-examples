public class Amphibian {


    private String am = "Stas";

    Amphibian() {
        System.out.println("Hello, I'm Amphibian");
    }

    @Override
    public String toString() {
        return "Amphibian{" +
                "am='" + am + '\'' +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize()");
        super.finalize();
    }

    protected void play() throws Throwable {
        System.out.println("PLAY FROG");
    }

    protected void tune(Amphibian amphibian) throws Throwable {
        amphibian.play();
    }
}

class Frog extends Amphibian {


    public Frog() {
        System.out.println("Hello, I'm Frog");
    }


    @Override
    protected void play() throws Throwable {
        try{super.play();
            System.out.println("new feature");
        }finally {
            finalize();
        }

    }


    public static void main(String[] args) {
        Frog frog = new Frog();
        try {
            frog.tune(frog);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        System.out.println(frog);

    }
}
